(defproject vocab-king "1.0.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://vocab-king.herokuapp.com"
  :license {:name "FIXME: choose"
            :url "http://example.com/FIXME"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.1.8"]
                 [ring/ring-jetty-adapter "1.2.2"]
                 [ring/ring-devel "1.2.2"]
                 [ring-basic-authentication "1.0.5"]
                 [environ "0.5.0"]                              ;;?
                 [com.cemerick/drawbridge "0.0.6"]                           ;;? remote repl?
                 [org.clojure/data.zip "0.1.1"]
                 [org.clojure/data.xml "0.0.7"]
                 [twitter-api "0.7.5"]
                 [prismatic/schema "0.2.2"]
                 [org.clojure/tools.trace "0.7.8"]
                 [org.clojure/java.jdbc "0.3.2"]
                 [postgresql "9.1-901.jdbc4"]
                 ]
  :min-lein-version "2.0.0"
  :plugins [[environ/environ.lein "0.2.1"]]
  :hooks [environ.leiningen.hooks]
  :uberjar-name "vocab-king-standalone.jar"
  :profiles {:production {:env {:production true}}})
