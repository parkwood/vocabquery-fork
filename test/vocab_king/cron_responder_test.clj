(ns vocab-king.cron-responder-test
  (:require
    [clojure.test :refer :all]
    [schema.test :as t]
    [vocab-king.cron-responder :as to-test]
    [vocab-king.twitter :as twitter]
    [vocab-king.dictionary-query :as vk]
    ))

(use-fixtures :once t/validate-schemas)

(deftest happy-simple
  (is (.endsWith
         (with-redefs [twitter/read-tweets (fn [x] [{:user "80s" :text "@lol colonel" :id 1} ])
                       twitter/tweet (fn [x y])
                       vk/get-defns-for-word (fn [x] [(get {"colonel" "darius jedburgh"} x)])]
                      (->
                        (doall (to-test/run! 1))
                        first
                        :to-tweet))
         "darius jedburgh")))

(deftest no-definitions
  (is (.endsWith
        (with-redefs [twitter/read-tweets (fn [x] [{:user "80s" :text "@lol colonel" :id 1} ])
                      twitter/tweet (fn [x y])
                      vk/get-defns-for-word (fn [x] [])]
                     (->
                       (doall (to-test/run! 1))
                       first
                       :to-tweet))
        "no definitions found for colonel")))

;; multi-defs, no defs. no query



