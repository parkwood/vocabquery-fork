(ns vocab-king.twitter
  (:use
    [twitter.oauth]
    [twitter.callbacks]
    [twitter.callbacks.handlers]
    [twitter.api.restful])
  (:import
    (twitter.callbacks.protocols SyncSingleCallback)))

(def my-creds (make-oauth-creds "QDYPtM68qVuCNZytoQxSvWXda"
                                "YzEzKx5ujeVG6hzkiZ750CgiSs1824eIKisTsHakUjNhTKOsTW"
                                "2546837126-LC5M6kT76LCwV1zRjGLM0l8XoRrK50CJIivfeWE"
                                "b62MVWiR5anxVghuemH4yIJGXZ9NYSvyy0lmUdPldKP8y"))

(defn read-tweets
  "responds with list of pairs of [tweeter, message, id]"
  [since]
  (->>
    (statuses-mentions-timeline :oauth-creds my-creds :params {:since_id since :count 200})
    :body
    (map (juxt (comp :screen_name :user) :text :id))
    (map #(zipmap [:user :text :id] %))))

(defn tweeted-tweets
  "responds with list of pairs of [tweeter, message, id]"
  []
  (->>
    (statuses-user-timeline :oauth-creds my-creds :params {:screen_name "vocabquery" :count 4000})
    :body
    (map (juxt (comp :screen_name :user) :text :id))
    (map #(zipmap [:user :text :id] %))))

(defn tweet [tweet in-reply-to]
  (let [trimmed-tweet (.substring tweet 0 (min (.length tweet) 139))]
    ;(println trimmed-tweet)
    (try
      (statuses-update :oauth-creds my-creds :params {:status trimmed-tweet :in-reply-to-status-id in-reply-to})
      (catch java.lang.Exception e (.getMessage e)))))
