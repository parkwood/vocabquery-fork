(ns vocab-king.dictionary-query
  (:require
    [schema.core :as s]
    [clojure.zip :as zip]
    [clojure.data.xml :as xml]
    [clojure.data.zip.xml :as zf]))

(def base-str "http://www.dictionaryapi.com/api/v1/references/sd3/xml/")
(def key-part "?key=695f9123-0a64-41ce-b791-9cbbdb4f0083")

(defn create-full-url [word]
  (str base-str word key-part))

;;(xml/parse-str (slurp "http://www.dictionaryapi.com/api/v1/references/sd3/xml/homogeneous?key=695f9123-0a64-41ce-b791-9cbbdb4f0083"))
(def homogenous "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n<entry_list version=\"1.0\">\n\t<entry id=\"homogeneous\"><hw>ho*mo*ge*neous</hw><sound><wav>homoge03.wav</wav></sound><pr>ˌhō-mə-ˈjē-nē-əs, -nyəs</pr><fl>adjective</fl><def><sn>1</sn><dt>:of the same or a similar kind or nature</dt><sn>2</sn><dt>:being the same throughout <vi>a culturally <it>homogenous</it> neighborhood</vi></dt></def><uro><ure>ho*mo*ge*ne*ity</ure> <sound><wav>homoge02.wav</wav><wpr>+hO-mu-ju-!nE-u-tE</wpr></sound><pr>-jə-ˈnē-ət-ē</pr> <fl>noun</fl></uro><uro><ure>ho*mo*ge*neous*ly</ure> <pr>-ˈjē-nē-əs-lē</pr> <fl>adverb</fl></uro></entry>\r\n</entry_list>")
(def not-found "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n<entry_list version=\"1.0\">\n\t</entry_list>")

(defn find-defs-in-zip [zip]
  (zf/xml-> zip :entry :def :dt zf/text))

(defn zip-str [str]
  (zip/xml-zip (xml/parse-str str)))

(defn get-defns-for-word                                    ;;:- [{}]
  [word]
  (try
    (->> word
        create-full-url
        slurp
        zip-str
        find-defs-in-zip
        (take 3))
    (catch java.lang.Exception e nil)))

