(ns vocab-king.cron-responder
  (:require
    [schema.core :as s]
    [vocab-king.twitter :as twitter]
    [vocab-king.dictionary-query :as dictionary]
    [clojure.string :as string :only [split]]
    [clojure.java.jdbc :as sql]))

(s/defn ^:always-validate extract-word :- (s/maybe s/Str)
  "find the first non @-starting word"
  [tweet :- s/Str]
  (some->>
    (string/split tweet #" ")
    (some #(when (.matches % "[A-Za-z]+") %))))

(s/defn map-defns-to-tweets                                 ;:- [{ :to-tweet s/Str :user s/Str :id s/Num }]
  [{:keys [query definitions] :as story} ]
  (if (empty? definitions)
    [(conj story [:to-tweet (str "no definitions found for " query)])]
    (map-indexed  #(conj story [:to-tweet (str query ": " (inc %1) " " %2)]) definitions)))

(defn prepare-response-tweets [tweets]
  (->>
    tweets
    (map #(conj % [:query (extract-word (:text %))]))
    (filter #(not (nil? (:query %))))
    (map #(conj % [:definitions (dictionary/get-defns-for-word (:query %))]))
    (mapcat map-defns-to-tweets)))

(defn run! [tweets-since]
  (->>
    tweets-since
    twitter/read-tweets
    (filter #(not= "vocabquery" (:user %)))
    prepare-response-tweets
    (map #(conj % [:result (twitter/tweet (str "@" (:user %) " " (:to-tweet %)) (:id %))]))
    ))

; postgres -D pg &
;"insert into tweet_data values (475371542676000768);"
;CREATE TABLE "tweet_data" ( last_tweet bigint primary key);
; select max(last_tweet) from tweet_data;
; \q
; heroku pg:psql

(def spec (or (System/getenv "DATABASE_URL")
              "postgresql://localhost:5432/twitter"))

(defn -main [& args]
  (some->>
    (sql/query spec
               ["select max(last_tweet) from tweet_data"])
       first
       :max
       run!
       (map :id)
       seq
       (apply max)
       (assoc {} :last_tweet)
       (sql/insert! spec :tweet_data)))